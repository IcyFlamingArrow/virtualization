# Copyright 2019 Julien Durillon <julien.durillon@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=NVIDIA tag="v${PV}" ]

SUMMARY="NVIDIA container runtime library"
DESCRIPTION="
Provides a library and a simple CLI utility to automatically configure GNU/Linux containers
leveraging NVIDIA hardware.
"

LICENCES="NVIDIA-OSS"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

# Update this version when bumping package.
# You can find the version in mk/nvidia-modprobe.mk
NVIDIA_MODPROBE_VERSION="396.51"
NVIDIA_MODPROBE_PREFIX="nvidia-modprobe-${NVIDIA_MODPROBE_VERSION}"

DOWNLOADS+="
    https://github.com/NVIDIA/nvidia-modprobe/archive/${NVIDIA_MODPROBE_VERSION}.tar.gz -> nvidia-modprobe-${NVIDIA_MODPROBE_VERSION}.tar.gz
"

DEPENDENCIES="
    build:
        sys-devel/bmake
        sys-apps/lsb-release
    build+run:
        sys-libs/libseccomp
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf )
"

MY_MAKE_OPTIONS=(
    REVISION="v${PV}"
    PLATFORM="$(uname -m)"
    WITH_LIBELF=yes
    OBJCPY="$(exhost --tool-prefix)objcopy"
    STRIP="$(exhost --tool-prefix)strip"
    prefix="/usr/$(exhost --target)"
    docdir=/usr/share/doc
)

src_unpack() {
    default

    # That ends the the download/unpack part of mk/nvidia-modprobe.mk
    local src_dir="${WORK}/deps/src/${NVIDIA_MODPROBE_PREFIX}"
    edo mkdir -p "${src_dir}"
    edo mv "${WORKBASE}/${NVIDIA_MODPROBE_PREFIX}/modprobe-utils" "${src_dir}"
    edo touch "${src_dir}/.download_stamp"
}

src_compile() {
    emake "${MY_MAKE_OPTIONS[@]}"
}

src_install() {
    DESTDIR=${IMAGE} emake install "${MY_MAKE_OPTIONS[@]}"
}
