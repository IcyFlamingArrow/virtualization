# Copyright 2009-2017 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="acpica-unix-${PV}"

SUMMARY="Intel ACPI Source Language (ASL) compiler/disassembler"
DESCRIPTION="iasl compiles ASL (ACPI Source Language) into AML (ACPI Machine
Language). This AML is suitable for inclusion as a DSDT in system
firmware. It also can disassemble AML, for debugging purposes."
HOMEPAGE="http://www.acpica.org"
DOWNLOADS="${HOMEPAGE}/sites/acpica/files/${MY_PNV}.tar.gz"

LICENCES="|| ( Intel BSD-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/bison[>=2.4.1]
        sys-devel/flex[>=2.5.4]
    build+run:
        !dev-lang/iasl [[
            description = [ dev-lang/iasl was a duplicate of sys-power/iasl and was buried ]
            resolution = uninstall-blocked-after
        ]]
"

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_COMPILE_PARAMS=( ACPI_HOST=_LINUX CC="${CC}" )
DEFAULT_SRC_INSTALL_PARAMS=( INSTALLFLAGS="-m 555" PREFIX="/usr/$(exhost --target)" )

src_prepare() {
    # Makefile doesn't provide any way to do a verbose build. Hack it.
    edo sed -i -e '/^\t@echo/d' -e 's/^\t@/\t/' generate/unix/Makefile.rules

    # Disable -Werror
    edo sed -i -e '/-Werror/d' generate/unix/Makefile.config

    default
}

