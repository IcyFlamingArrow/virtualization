# Copyright 2009-2019 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}-emu

require sourceforge [ suffix=tar.gz project=${MY_PN} pn=${PN} ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_prepare

SUMMARY="The Versatile Commodore Emulator (VICE)"
DESCRIPTION="
VICE is a program that runs on about any platform and executes programs intended
for Commodore's old but legendary 8-bit computers. The current version emulates
the C64, the C64DTV, the C128, the VIC20, almost all PET models, the PLUS4 and
the CBM-II (aka C610).
If you want to emulate either of those classic computers, don't look any further,
VICE is for you.
"
HOMEPAGE="http://${MY_PN}.sourceforge.net"

UPSTREAM_CHANGELOG="http://${MY_PN}.svn.sourceforge.net/viewvc/${MY_PN}/tags/v${PV}/v${PV}/${PN}/ChangeLog"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/plain/NEWS"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/vice_toc.html [[ lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    alsa
    debug [[ description = [ Enable debug source options ] ]]
    ethernet [[ description = [ Emulate a The Final Ethernet net adapter for the C64 ] ]]
    gtk3
    pulseaudio
    sdl
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( gtk3 sdl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-text/dos2unix
        dev-util/pkg-config
        sys-devel/gettext
        x11-apps/bdftopcf
        x11-apps/mkfontscale[>=1.2.0]
    build+run:
        dev-lang/xa
        dev-libs/glib:2
        media/ffmpeg[>=2.5]
        media-libs/flac
        media-libs/giflib:=
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/x264
        media-sound/mpg123
        sys-apps/pciutils
        alsa? ( sys-sound/alsa-lib )
        ethernet? ( dev-libs/libpcap )
        gtk3? (
            media-libs/glew
            x11-dri/mesa [[ note = [ provides libGL ] ]]
            x11-libs/cairo[>=0.5.1]
            media-libs/fontconfig[>=2.0.0]
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.22][X]
            x11-libs/pango
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.21] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:2[X] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=enable-fast-install
    --hates=disable-silent-rules
    --prefix=/usr
    --bindir=/usr/$(exhost --target)/bin
    --verbose
    --enable-cpuhistory
    --enable-ethernet
    --enable-external-ffmpeg
    --enable-inline
    --enable-ipv6
    --enable-midi
    --enable-native-tools
    --enable-parsid
    --enable-realdevice
    --enable-rs232
    --disable-embedded
    --disable-lame
    --disable-libieee1284
    --disable-portaudio
    --disable-quicktime
    --disable-sdlui
    --with-gif
    --with-jpeg
    --with-png
    --with-readline
    --with-resid
    --with-zlib
    --without-arts
    --without-oss
    --without-x
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    ethernet
    "gtk3 native-gtk3ui"
    "sdl sdlui2"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    alsa
    "pulseaudio pulse"
    "sdl sdlsound"
)

vice_src_prepare() {
    default

    edo sed -i -e "s:/lib64/:/$(exhost --target)/lib/:" configure.ac

    # This check messes ar up so just disable - it only suppresses a warning anyway
    export ar_check=no

    eautoconf
}

